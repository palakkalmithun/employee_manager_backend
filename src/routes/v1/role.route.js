const express = require('express')
const rolesController = require('../../controllers/roles.controller')

const router = express.Router()

console.log('reached role router')

// router.post('/', rolesController.createRole)

router
  .route('/')
  .get(rolesController.getRoles)
  .post(rolesController.createRole)

router
  .route('/:id')
  .get(rolesController.getRoleById)
  .delete(rolesController.removeRoleById)


module.exports = router
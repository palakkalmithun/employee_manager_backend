const express = require('express')
const projectController = require('../../controllers/projects.controller')

const router = express.Router()


router
  .route('/')
  .get(projectController.getProjects)
  .post(projectController.createProject)

router
  .route('/:id')
  .get(projectController.getProjectById)
  .put(projectController.updateProject)
  .delete(projectController.removeProjectById)



module.exports = router
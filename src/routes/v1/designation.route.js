const express = require('express')
const designationsController = require('../../controllers/designation.controller')

const router = express.Router()


router
  .route('/')
  .get(designationsController.getDesignations)
  .post(designationsController.createDesignation)

router.route('/:id')
  .get(designationsController.getDesignationById)
  .delete(designationsController.removeDesignationById)


module.exports = router
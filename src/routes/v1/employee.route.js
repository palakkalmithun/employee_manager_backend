const express = require('express')
const employeesController = require('../../controllers/employees.controller')

const router = express.Router()


router
  .route('/')
  .get(employeesController.getEmployees)
  .post(employeesController.createEmployee)


router
  .route('/:id')
  .get(employeesController.getEmployeeById)
  .put(employeesController.updateEmployeeById)
  .delete(employeesController.removeEmployeeById)


module.exports = router
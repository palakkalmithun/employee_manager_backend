const express = require('express')
const empRoute = require('./employee.route')
const designationRoute = require('./designation.route')
const roleRoute = require('./role.route')
const projectRoute = require('./project.route')

const router = express.Router()

const defaultRoutes = [
  {
    path: '/emp',
    route: empRoute
  },
  {
    path: '/des',
    route: designationRoute
  },
  {
    path: '/rol',
    route: roleRoute
  },
  {
    path: '/pro',
    route: projectRoute
  }
]
defaultRoutes.forEach((route) => {
  router.use(route.path, route.route)
})


module.exports = router
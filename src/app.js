const express = require('express')
const helmet = require('helmet')
const xss = require('xss-clean')
const mongoSanitize = require('express-mongo-sanitize')
const cors = require('cors')
const routes = require('./routes/v1')
const errorHandler = require('./middlewares/error')
const ApiError = require('./utilss/ApiError')
const httpStatus = require('http-status')

const app = express()



app.use(helmet())

app.use(express.json())

app.use(xss())
app.use(mongoSanitize())

app.use(cors())
app.options('*', cors())


app.use('/api', routes)

app.use((req, res, next) => {
  next(new ApiError(httpStatus.NOT_FOUND, 'Not found'))
})

app.use(errorHandler)

module.exports = app
const httpStatus = require('http-status')
const Role = require('../models/role')
const ApiError = require('../utilss/ApiError')


const getRoles = async () => {
  const roles = await Role.find({})
  return roles
}

const getRoleById = async (id) => {
  const role = await Role.findById(id)
  return role
}

const createRole = async (newRole) => {
  console.log('newrole:',newRole)
  const savedRole = await Role.create(newRole)
  return savedRole
}

const removeRoleById = async (id) => {
  const role = await getRoleById(id)
  if (!role) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Role not found, Unable to delete')
  }
  await role.remove()
  return role
}

module.exports = {
  getRoles,
  getRoleById,
  createRole,
  removeRoleById
}
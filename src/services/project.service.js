const httpStatus = require('http-status')
const Project = require('../models/project')
const ApiError = require('../utilss/ApiError')



const getProjects = async () => {
  const projects = await Project.find({})
  return projects
}

const getProjectById = async (id) => {
  const project = await Project.findById(id)
  return project
}

const createProject = async (newProject) => {
  const savedProject = await Project.create(newProject)
  return savedProject
}

const removeProjectById = async (id) => {
  const project = await getProjectById(id)
  if (!project) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Project not found, Unable to delete')
  }
  await project.remove()
  return project
}

const updateProject = async (id, projectObject) => {
  const project = await getProjectById(id)
  if (!project) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Project not found, unable to update')
  }
  Object.assign(project, projectObject)
  await project.save()
  return project
}

module.exports = {
  getProjects,
  getProjectById,
  createProject,
  removeProjectById,
  updateProject
}
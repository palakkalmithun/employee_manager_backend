const httpStatus = require('http-status')
const Employee = require('../models/employee')
const ApiError = require('../utilss/ApiError')

const createEmployee = async (userBody) => {
  const newEmployee = await Employee.create(userBody)
  return newEmployee
}

const getEmployeeById = async (id) => {
  const employee = Employee.findById(id)
  return employee
}

const getEmployees = async () => {
  const employees = await Employee.find({}) //need to populate ..check and populate
  return employees
}

const updateEmployeeById = async (id, employeeObject) => {
  const employee = await getEmployeeById(id)
  if (!employee) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found')
  }
  Object.assign(employee, employeeObject)
  await employee.save()
  return employee
}

const removeEmployeeById = async (id) => {
  const employee = await getEmployeeById(id)
  if (!employee) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not foound')
  }
  await employee.remove()
  return employee
}

module.exports = {
  createEmployee,
  getEmployeeById,
  getEmployees,
  updateEmployeeById,
  removeEmployeeById,
}
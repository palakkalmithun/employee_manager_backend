const httpStatus = require('http-status')
const Designation = require('../models/designation')
const ApiError = require('../utilss/ApiError')


const getDesignations = async () => {
  const designations = await Designation.find({})
  return designations
}

const getDesignationById = async (id) => {
  const designation = await Designation.findById(id)
  return designation
}

const createDesignation = async (newDesignation) => {
  const savedDesignation = await Designation.create(newDesignation)
  return savedDesignation
}

const removeDesignationById = async (id) => {
  const designation = await getDesignationById(id)
  if (!designation) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Designation not found, Unable to delete')
  }
  await designation.remove()
  return designation
}




module.exports = {
  getDesignations,
  getDesignationById,
  createDesignation,
  removeDesignationById
}
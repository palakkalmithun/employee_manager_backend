const httpStatus = require('http-status')
const projectService = require('../services/project.service')
const ApiError = require('../utilss/ApiError')
const catchAsync = require('../utilss/catchAsync')


const getProjects = catchAsync(async (req, res) => {
  const projects = await projectService.getProjects()
  res.json(projects)
})

const getProjectById = catchAsync(async (req, res) => {
  const project = await projectService.getProjectById(req.params.id)
  if (!project) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Project not found')
  }
  res.json(project)
})

const createProject = catchAsync(async (req, res) => {
  const savedProject = await projectService.createProject(req.body)
  res.json(savedProject)
})

const removeProjectById = catchAsync(async (req, res) => {
  await projectService.removeProjectById(req.params.id)
  res.status(httpStatus.NO_CONTENT).end()
})

const updateProject = catchAsync(async (req, res) => {
  const savedProject = await projectService.updateProject(req.params.id, req.body)
  res.json(savedProject)
})


module.exports = {
  getProjects,
  getProjectById,
  createProject,
  removeProjectById,
  updateProject
}
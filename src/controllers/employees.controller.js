const httpStatus = require('http-status')
const ApiError = require('../utilss/ApiError')
const catchAsync = require('../utilss/catchAsync')
const empService = require('../services/employee.service')

const createEmployee = catchAsync(async (req, res) => {
  const user = await empService.createEmployee(req.body)
  res.json(user)
})

const getEmployeeById = catchAsync(async (req, res) => {
  const savedEmployee = await empService.getEmployeeById(req.params.id)
  if (!savedEmployee) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found')
  }
  res.json(savedEmployee)
})

const getEmployees = catchAsync(async (req, res) => {
  const employees = await empService.getEmployees()
  res.json(employees)
})

const updateEmployeeById = catchAsync(async (req, res) => {
  const savedEmployee = await empService.updateEmployeeById(req.params.id, req.body)
  res.json(savedEmployee)
})

const removeEmployeeById = catchAsync(async (req, res) => {
  await empService.removeEmployeeById(req.params.id)
  res.status(httpStatus.NO_CONTENT).end()
})


module.exports = {
  createEmployee,
  getEmployeeById,
  getEmployees,
  updateEmployeeById,
  removeEmployeeById,
}
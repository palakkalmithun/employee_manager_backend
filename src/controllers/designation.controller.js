const httpStatus = require('http-status')
const ApiError = require('../utilss/ApiError')
const catchAsync = require('../utilss/catchAsync')
const designationService = require('../services/employee.service')


const getDesignations = catchAsync(async (req, res) => {
  const designations = await designationService.getDesignations()
  res.json(designations)
})

const getDesignationById = catchAsync(async (req, res) => {
  const designation = await designationService.getDesignationById(req.params.id)
  if (!designation) {
    throw new ApiError(httpStatus.NOT_FOUND, 'designation not found')
  }
  res.json(designation)
})

const createDesignation = catchAsync(async (req, res) => {
  const savedDesignation = await createDesignation(req.body)
  res.json(savedDesignation)
})

const removeDesignationById = catchAsync(async (req, res) => {
  await designationService.removeDesignationById(req.params.id)
  res.status(httpStatus.NO_CONTENT).end()
})


module.exports = {
  getDesignations,
  getDesignationById,
  createDesignation,
  removeDesignationById
}
const httpStatus = require('http-status')
const roleService = require('../services/role.service')
const catchAsync = require('../utilss/catchAsync')


const getRoles = catchAsync(async (req, res) => {
  const roles = await roleService.getRoles()
  res.json(roles)
})

const getRoleById = catchAsync(async (req, res) => {
  const role = await roleService.getRoleById(req.params.id)
  res.json(role)
})

const createRole = catchAsync(async (req, res) => {
  const savedRole = await roleService.createRole(req.body)
  res.json(savedRole)
})

const removeRoleById = catchAsync(async (req, res) => {
  await roleService.removeRoleById(req.params.id)
  res.status(httpStatus.NO_CONTENT).end()
})

module.exports = {
  getRoles,
  getRoleById,
  createRole,
  removeRoleById
}
const mongoose = require('mongoose')

const projectSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true
  },
  plannedStartDate: {
    type: String,
    required: true,
  },
  plannedEndDate: {
    type: String,
    required: true
  },
  actualStartDate: {
    type: String,
  },
  actualEndDate: {
    type: String,
  },
  team: [
    {
      name: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
      },
      role: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Role',
      }
    }
  ]

},
{
  timestamps: true,
}
)

projectSchema.set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id.toString()
    delete ret._id
    delete ret.__v
    delete ret.createdAt
    delete ret.updatedAt
  }
})

module.exports = mongoose.model('Project', projectSchema)
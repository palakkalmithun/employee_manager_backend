const mongoose = require('mongoose')

const designationSchema = new mongoose.Schema({
  designation: {
    type: String,
    unique: true,
    required: true,
  },
  employees: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Employee'
  }]
},
{
  timestamps: true,
}
)

designationSchema.set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id.toString()
    delete ret._id
    delete ret.__v
    delete ret.createdAt
    delete ret.updatedAt
  }
})


module.exports = mongoose.model('Designation', designationSchema)
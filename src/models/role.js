const mongoose = require('mongoose')


const roleSchema = new mongoose.Schema({
  role: {
    type: String,
    required: true,
    unique: true
  },
  employees: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Employee'
  }]
},
{
  timestamps: true,
}
)


roleSchema.set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id.toString()
    delete ret._id
    delete ret.__v
    delete ret.createdAt
    delete ret.updatedAt
  }
})



module.exports = mongoose.model('Role', roleSchema)
const mongoose = require('mongoose')

const employeeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    unique: true,
    trim: true,
    lowercase: true,
  },
  passwordHash: {
    type: String,
    required: true
  },
  contactNo: {
    type: String,
    unique: true,
    trim: true
  },
  joinDate: {
    type: String,
    required: true,
  },
  designation: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Designation'
  },
  role: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Role'
  }
},
{
  timestamps: true,
}
)


// add  role ,

employeeSchema.set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id.toString()
    delete ret._id
    delete ret.__v
    delete ret.passwordHash
    delete ret.createdAt
    delete ret.updatedAt

  }
})

module.exports = mongoose.model('Employee', employeeSchema)